package pt.unl.fct.di.apdc.apdcwork.util;

public class RegisterData {

	public String username;
	public String name;
	public String email;	
	public String password;
	public String confirmation;
	public String mobile;
	public String fixedLine;
	public String nif;
	public String street;
	public String complement;
	public String locality;
	public String postcode;
	
	public RegisterData() {
		
	}
	
	public RegisterData(String username, String name, String email, String password, 
						String confirmation, String mobile, String fixedLine, String nif, String street,
						String complement, String locality, String postcode) {
		
			this.username = username;
			this.name = name;
			this.email = email;
			this.password = password;
			this.confirmation = confirmation;
			this.mobile = mobile;
			this.fixedLine = fixedLine;
			this.nif = nif;
			this.street = street;
			this.complement = complement;
			this.locality = locality;
			this.postcode = postcode;
	}
	
	private boolean validField(String value) {
		return value != null && !value.equals("");
	}
	/*
	private boolean validPNumber(int  value){
		return (value>=90000000 && value<=999999999) || (value>=20000000 && value<=299999999);
	}*/
	
	
	public boolean validRegistration() {
		return validField(username) &&
			   validField(name) &&
			   validField(email) &&
			   validField(password) &&
			   validField(confirmation) &&
			   password.equals(confirmation) &&
			   email.contains("@") &&
			   validField(mobile) &&
			   validField(fixedLine) &&
			   validField(nif) &&
			   validField(street) &&
			   validField(locality) &&
			   validField(postcode);
		
	}
	
}
