package pt.unl.fct.di.apdc.apdcwork.resources;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.gson.Gson;

@Path ("/maps")
public class SearchUsersResource {

	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	public SearchUsersResource() {
		//Nothing to be done here...
	}
	
	@GET
	@Path ("/{id}/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response searchUserAddress (@PathParam("id") String id, @Context HttpServletRequest request,@Context HttpHeaders headers) throws Exception {
		Key userKey = KeyFactory.createKey("User", id);
		try{
			Entity user = datastore.get(userKey);
			return Response.ok(g.toJson(user.getProperty("user_street"))).build();
		} catch (EntityNotFoundException e) {
			return Response.status(Status.FORBIDDEN).build();
		}
	}
}
