package pt.unl.fct.di.apdc.apdcwork.resources;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.apdcwork.util.AuthToken;

@Path("/login")
@Produces(MediaType.APPLICATION_JSON +";charset=utf-8")
public class LoginResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	private final Gson g = new Gson();
	public LoginResource(){
		//Nothing to be done here...
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doLogin(pt.unl.fct.di.apdc.apdcwork.util.LoginData data) {
		LOG.fine("Attempt to login user: " + data.username);
		
		Key userKey = KeyFactory.createKey("User", data.username);
		try {
			Entity user = datastore.get(userKey);
			String hashedPWD = (String) user.getProperty("user_pwd");
			if (hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {
				AuthToken token = new AuthToken(data.username);
				LOG.info("User '" + data.username + "' logged in sucessfully.");
				Entity token1 = new Entity("Token", token.tokenID);
				token1.setProperty("user_name", token.username);
				token1.setProperty("user_creationData", token.creationData);
				token1.setProperty("user_expirationData", token.expirationData);
				datastore.put(token1);
				return Response.ok(g.toJson(token)).build();				
			} else {
				LOG.warning("Wrong password for username: " + data.username);
				return Response.status(Status.FORBIDDEN).build();				
			}
		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed login attempt for username: " + data.username);
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	@Path("/user")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response checkUsernameAvailable( pt.unl.fct.di.apdc.apdcwork.util.LoginData data ) {
		
		Key userKey = KeyFactory.createKey("User", data.username);
		try {
			Entity user = datastore.get(userKey);
			String hashedPWD = (String) user.getProperty("user_pwd");
			if (hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {

				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, -1);
				Date yesterday = cal.getTime();
				
				// Obtain the user login statistics
				Filter propertyFilter =
					    new FilterPredicate("user_login_time", FilterOperator.GREATER_THAN_OR_EQUAL, yesterday);
				Query ctrQuery = new Query("UserLog").setAncestor(KeyFactory.createKey("User", data.username))
						             .setFilter(propertyFilter)
						             .addSort("user_login_time", SortDirection.DESCENDING);
				ctrQuery.addProjection(new PropertyProjection("user_login_time", Date.class));
				ctrQuery.addProjection(new PropertyProjection("user_login_ip", String.class));
				List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withLimit(3));
				
			  /*List<Date> loginDates = new ArrayList();
				for(Entity userlog:results) {
						loginDates.add((Date) userlog.getProperty("user_login_time"));
				}*/				
				return Response.ok(g.toJson(results)).build();
			
			} else {
				LOG.warning("Wrong password for username: " + data.username );
				return Response.status(Status.FORBIDDEN).build();				
			}
		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed login attempt for username: " + data.username);
			return Response.status(Status.FORBIDDEN).build();
		}
	}
	
}
